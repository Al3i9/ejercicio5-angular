import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-condicional',
  templateUrl: './condicional.component.html',
  styleUrls: ['./condicional.component.css']
})
export class CondicionalComponent implements OnInit {

  datos = {
    nombreCompleto : 'Alizon Melany Condori Morales',
    direccion: 'calle venezuela'
  };

  
  mostrar = true;

  constructor() { }

  ngOnInit(): void {
  }

}

